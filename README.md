# Managementrh

## Description 
### structure du projet

Dans la structure du projet, vous retrouverez le schéma suivant :

* Pour chaque composant, il y a dans son repertoire des composants dont l'usage lui est exclusif ; et les composants utilisés par plusieurs autres composants, sont au même niveau. 

* Hiérarchie des composants dans les dossiers du projet :
  * Accueil
    * ListHeader
    * MyeventsList : Liste des évènements
      * EventItem
    * MyrequestsList : Liste des demandes
      * RequestItem
      * RequestTitle
    * Searchbar
  * Header
    * title
    * toolbar
  * Navbar
    * Logo
    * MenuTab

En ce qui concerne l'ordre d'appel, ou d'utilisation, on a tout d'abord le composant Accueil qui fait appel aux composants Navbar, Header, MyrequestsList et MyeventsList ; ces derniers ensuite font appels aux composants au bas de leur hiérarchie.
Les composants ListHeader et Searchbar sont à ce niveau hierarchique, du fait qu'ils sont utilisés par les composants du même niveau de hiérarchie (MyeventsList, MyrequestsList).

### Les données
A titre d'exemple, les données ont été mises dans un Array dans le service principal (services/MyServiceService). Il s'agit d'un simple objet qui est représenté par les objets retrouvés dans le dossier models.

### Autres informations
* Bootstrap et Angular material ont été utilisés
* La plus part des icons proviennent de flaticon (www.flaticon.com/free-icons/)

### Angular version
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.15.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
