import {Demande} from './demande.model';
import {Evenement} from './evenement.model';

export class Collaborateur {
  id: number;
  nom: string;
  prenom: string;
  photoProfilSrc: string;
  nbrNotifications: number;
  demandes: Demande [] = [];
  evenements: Evenement [] = [];
}
