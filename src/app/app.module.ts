import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { SharedModulesModule } from './shared-modules/shared-modules.module';

import { AppComponent } from './app.component';
import { AccueilComponent } from './accueil/accueil.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HeaderComponent } from './header/header.component';
import { TitleComponent } from './header/title/title.component';
import { ToolbarComponent } from './header/toolbar/toolbar.component';
import { MyrequestsListComponent } from './accueil/myrequests-list/myrequests-list.component';
import { MyeventsListComponent } from './accueil/myevents-list/myevents-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EventItemComponent } from './accueil/myevents-list/event-item/event-item.component';
import { RequestItemComponent } from './accueil/myrequests-list/request-item/request-item.component';
import { SearchbarComponent } from './accueil/searchbar/searchbar.component';
import {FormsModule} from '@angular/forms';
import { RequestTitleComponent } from './accueil/myrequests-list/request-title/request-title.component';
import { LogoComponent } from './navbar/logo/logo.component';
import { MenuTabComponent } from './navbar/menu-tab/menu-tab.component';
import { ListHeaderComponent } from './accueil/list-header/list-header.component';

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    NavbarComponent,
    HeaderComponent,
    TitleComponent,
    ToolbarComponent,
    MyrequestsListComponent,
    MyeventsListComponent,
    EventItemComponent,
    RequestItemComponent,
    SearchbarComponent,
    RequestTitleComponent,
    LogoComponent,
    MenuTabComponent,
    ListHeaderComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        SharedModulesModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
