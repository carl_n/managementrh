import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-request-title',
  templateUrl: './request-title.component.html',
  styleUrls: ['./request-title.component.css']
})
export class RequestTitleComponent implements OnInit {
  @Input() titre: string;
  @Input() imgSrc: string;
  constructor() { }

  ngOnInit(): void {
  }

}
