import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestTitleComponent } from './request-title.component';

describe('RequestTitleComponent', () => {
  let component: RequestTitleComponent;
  let fixture: ComponentFixture<RequestTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
