import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyrequestsListComponent } from './myrequests-list.component';

describe('MyrequestsListComponent', () => {
  let component: MyrequestsListComponent;
  let fixture: ComponentFixture<MyrequestsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyrequestsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyrequestsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
