import { Injectable } from '@angular/core';

import {Collaborateur} from '../models/collaborateur.model';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MyServiceService {
  collaborateurs: Collaborateur[] = [
      {
        id: 0,
        nom: 'EL ALAMI',
        prenom: 'Yassine',
        photoProfilSrc: 'assets/imgs/homme.png',
        nbrNotifications: 2,
        demandes: [
          {
            id: 0,
            titre: 'Attestation de salaire',
            type: 'document',
            etat: 'En cours'
          },
          {
            id: 1,
            titre: 'Attestation de salaire',
            type: 'document',
            etat: 'Complet'
          },
          {
            id: 2,
            titre: 'Congé spécials Mariage (4jours)',
            type: 'conges',
            etat: 'Refuse'
          },
          {
            id: 3,
            titre: 'Congé Annuel (12jours)',
            type: 'conges',
            etat: 'Complet'
          },
          {
            id: 4,
            titre: 'Arrêt maladie (4 jours)',
            type: 'absences',
            etat: 'En cours'
          },
          {
            id: 5,
            titre: 'Permis de conduire (1 jour)',
            type: 'absences',
            etat: 'Complet'
          }
        ],
        evenements: [
          {
            id: 0,
            titre: 'Reading And Sharing',
            description: 'Join Flatiron School as one of our lead product design instructors demonstrates the basic fundamentals of... UX Design. Visual designers make websites look ... beautiful. UX designers make them work. Join us and learn the fundamentals of a user-centric approach and practice empathy based problem solving. What You’ll Learn: Learn about UX Design and how it’s used to create meaningful experiences between people and digital applications. Practice solving problems through empathy Discover free resources to help you develop user-centric solutions to problems. Increase your confidence in your design thinking and bring your ideas. Who is the workshop for? ',
            date: '2022-01-08',
            duree: '5h30',
            lieu: 'Bd Ziraoui, Casablanca 20250',
            imageSrc: 'assets/imgs/sharing.jpeg'
          },
          {
            id: 1,
            titre: 'Soccer Tournament',
            description: 'Join Flatiron School as one of our lead product design instructors demonstrates the basic fundamentals of... UX Design. Visual designers make websites look ... beautiful. UX designers make them work. Join us and learn the fundamentals of a user-centric approach and practice empathy based problem solving. What You’ll Learn: Learn about UX Design and how it’s used to create meaningful experiences between people and digital applications. Practice solving problems through empathy Discover free resources to help you develop user-centric solutions to problems. Increase your confidence in your design thinking and bring your ideas. Who is the workshop for? ',
            date: '2022-01-08',
            duree: '5h30',
            lieu: 'Bd Ziraoui, Casablanca 20250',
            imageSrc: 'assets/imgs/soccer.jpg'
          },
          {
            id: 2,
            titre: 'Le Loup Garou',
            description: 'Join Flatiron School as one of our lead product design instructors demonstrates the basic fundamentals of... UX Design. Visual designers make websites look ... beautiful. UX designers make them work. Join us and learn the fundamentals of a user-centric approach and practice empathy based problem solving. What You’ll Learn: Learn about UX Design and how it’s used to create meaningful experiences between people and digital applications. Practice solving problems through empathy Discover free resources to help you develop user-centric solutions to problems. Increase your confidence in your design thinking and bring your ideas. Who is the workshop for? ',
            date: '2022-01-08',
            duree: '5h30',
            lieu: 'Bd Ziraoui, Casablanca 20250',
            imageSrc: 'assets/imgs/loup.jpg'
          },
          {
            id: 3,
            titre: 'Hors riding',
            description: 'Join Flatiron School as one of our lead product design instructors demonstrates the basic fundamentals of... UX Design. Visual designers make websites look ... beautiful. UX designers make them work. Join us and learn the fundamentals of a user-centric approach and practice empathy based problem solving. What You’ll Learn: Learn about UX Design and how it’s used to create meaningful experiences between people and digital applications. Practice solving problems through empathy Discover free resources to help you develop user-centric solutions to problems. Increase your confidence in your design thinking and bring your ideas. Who is the workshop for? ',
            date: '2022-01-08',
            duree: '5h30',
            lieu: 'Bd Ziraoui, Casablanca 20250',
            imageSrc: 'assets/imgs/Horse-riding.jpg'
          }
        ]
      }
    ];
  collaborateursSubject = new Subject<Collaborateur[]>();

  constructor() {
  }

  emitCollaborateurs(){
    this.collaborateursSubject.next(this.collaborateurs.slice());
  }

  findCollaborateurById(id: number){
    let collaborateur = null;
    for (const collab of this.collaborateurs) {
      if (collab.id === id) {
        collaborateur = collab;
        break;
      }
    }
    return collaborateur;
  }

  getCollaborateurEvenements(id: number){
    const collaborateur = this.findCollaborateurById(id);
    return (collaborateur != null) ? collaborateur.evenements : null;
  }

  getCollaborateurDemandes(id: number){
    const collaborateur = this.findCollaborateurById(id);
    return (collaborateur != null) ? collaborateur.demandes : null;
  }
}
