import {Component, Input, OnInit} from '@angular/core';
import {Collaborateur} from '../models/collaborateur.model';
import {Subscription} from 'rxjs';
import {MyServiceService} from '../services/my-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  title: any = 'Bonjour ';
  @Input() index: any;
  collaborateur: Collaborateur;
  collaboteursSubscription: Subscription;

  constructor(private myMervice: MyServiceService) {
  }

  ngOnInit(): void {
    this.collaboteursSubscription = this.myMervice.collaborateursSubject.subscribe(collaborateurs => {
      this.collaborateur = collaborateurs[this.index];
      console.log('****Collaborateur***:', this.collaborateur);
    });
    this.myMervice.emitCollaborateurs();

    this.title = this.title + this.collaborateur.prenom;
  }

}
